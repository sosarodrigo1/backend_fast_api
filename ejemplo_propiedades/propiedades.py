#### imports ####
import uvicorn
from fastapi import FastAPI, Response
from api.propiedades_api import apiprops #Importamos la instancia de APIRouter

#### Programa ####
app = FastAPI()
app.include_router(apiprops) #vinculamos el contenedor de apis que está en "propiedades_api"

#Solo ubico acá el endpoint de inicio
@app.get('/') #Pagina Inicio
def home():
    return Response('Home page', 200)

if __name__ == "__main__":
    uvicorn.run('propiedades:app',reload=True)
