#### Imports ####
from pydantic import BaseModel
from typing import Optional
import datetime

#### Modelos ####
class PropiedadBase(BaseModel):
    #https://pydantic-docs.helpmanual.io/usage/types/ (VER)
    #En general el id no se usa en la propiedadBase porque lo asigna el servidor
    direccion: str
    estado: Optional[str] #Optional de pydantic
    tipo: str = "Casa" #Por defecto asigna "Casa"

class PropiedadSinId(PropiedadBase): #Utilizado para el post
     habitaciones: int = None #Es otra alternativa a Optional darle un valor None x defecto
     fecha_construccion: Optional[datetime.date] #Formato utilizado para fechas en pydantic (pydantic-docs)

class Propiedad(PropiedadSinId):
    #Se utiliza solo para salidas, entonces no necesita validaciones de entrada
    id: int

class PropiedadLista(PropiedadBase):
    #Se utiliza solo para salidas, entonces no necesita validaciones de entrada
    id: int #Solo agregado para ver los id de la lista y comprobar, luego sacar.
    #pass (Luego de sacar el id solo colocamos el pass)