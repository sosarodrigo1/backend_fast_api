#### Imports ####
from fastapi import Response,status, APIRouter
from modelo.propiedades_modelo import Propiedad, PropiedadSinId, PropiedadLista
from db import propiedades, nuevo_id, buscar_propiedad

#Para organizar/separar el código FastAPI nos ofrece la herramienta APIRouter
#que sirve para agrupar endpoints. Declaramos una variable "apiprops" por ej. e instanciamos 
#un APIRouter, esta variable nos sirve para contener los endpoints, métodos que respondan 
#a URLS, de esta forma le decimos que todas estas api (endpoints) están dentro de 
#ese APIRouter, en esa instancia llamada apiprops:

apiprops = APIRouter(prefix='/propiedades') #Luego esto tenemos que vincularlo al "app" que está en el main
#el atributo prefix establece un prefijo para los endpoints ya que generalmente
#se repiten ej: propiedades/ contactos/ autos/ etc.

#el prefix se ubica en el primer parámeto del endpoint '' = '/propiedades':
@apiprops.get('', response_model=list[PropiedadLista])#Devuelve con el formato de Model "PropiedadLista"
def get_all():
    return propiedades

@apiprops.get('/{id}', response_model=Propiedad) #Devuelvo con modelo completo (Propiedad)
def get_propiedad(id: int):
    p = buscar_propiedad(id)
    if p == None:
        return Response('Propiedad no encontrada', status.HTTP_404_NOT_FOUND)
    return p

@apiprops.post('') #Crea propiedades
def create_propiedades(nueva_propiedad:PropiedadSinId): #Viene una Propiedad sin id
#Creo una instancia de Propiedad y le asigno el id que retorna la función nuevo_id()
#y asigno los valores que vienen de la request.
    p = Propiedad(id=nuevo_id(), #Asigna un id
        direccion=nueva_propiedad.direccion,
        estado=nueva_propiedad.estado,
        habitaciones=nueva_propiedad.habitaciones,
        fecha_construccion=nueva_propiedad.fecha_construccion,
        tipo=nueva_propiedad.tipo)
    #Agrega la propiedad creada a la lista
    propiedades.append(p)
    #Retorna con un mensaje y un código 201
    return Response('Propiedad agregada correctamente!', status_code=201)

@apiprops.delete('/{id}') #Borra Propiedades
def delete_propiedad(id:int):
    try:
        p = buscar_propiedad(id)
        if not p:
            return Response(content='No se encuentra la propiedad', status_code=404)
        
        propiedades.remove(p)
        return Response(content='Propiedad borrada correctamente', status_code=200)
    except Exception as ex:
        return Response(content='Error al borrar la propiedad' + str(ex), status_code=500)

@apiprops.put('/{id}') #Modifica Propiedades
def update_propiedad(id:int, item: PropiedadSinId):
    try:
        p: Propiedad = buscar_propiedad(id)
        if not p:
            return Response(content='No se encuentra la propiedad', status_code=404)
    
        p.direccion=item.direccion,
        p.estado=item.estado,
        p.habitaciones=item.habitaciones,
        p.fecha_construccion=item.fecha_construccion,
        p.tipo=item.tipo
    
        return Response('Propiedad modificada correctamente!', status_code=200)
    except Exception as ex:
        return Response(content='Error al modificar la propiedad' + str(ex), status_code=500)
