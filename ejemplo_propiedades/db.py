#### Imports ####
from modelo.propiedades_modelo import Propiedad
import datetime

#### Datos ####
propiedades = [
    Propiedad(id=1, direccion='Urquiza 494', estado='A estrenar', habitaciones=1, fecha_construccion = datetime.date(1980,7,5), tipo='Departamento'),
    Propiedad(id=2, direccion='Laprida 83', estado='Antigua', habitaciones=2, fecha_construccion = None, tipo='Departamento'),
    Propiedad(id=3, direccion='España 8974', estado='Muy bueno', habitaciones=3, fecha_construccion = datetime.date.today(), tipo='Casa')
    ]

#### Funciones internas ####
def buscar_propiedad(id: int):
#Busca la propiedad en la lista por si id, si no encuentra el id devuelve None
    return next((x for x in propiedades if x.id == id), None) #Los () crean un generador,
    #la diferencia es que no se crea una nueva lista en la memoria, si no q espera al método
    # next(), busca valor x valor, y si no lo encuentra asigna el valor x defecto, None en este caso

#Crea un nuevo id buscando el id + alto y sumando 1
def nuevo_id():
    resultado = 0
    for p in propiedades:
        if p.id>resultado:
            resultado = p.id
    return resultado+1
