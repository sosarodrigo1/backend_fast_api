from imp import reload
from contacto import ContactoCreate, Contacto, ContactoLista
import uvicorn
from fastapi import FastAPI, Response,status
from store import contactos

app = FastAPI()

if __name__ == "__main__":
    uvicorn.run(f"{__name__}:app", reload=True)


#Devuelvo la lista de contactos completa
#Ruta (Por convensión en plural)
#Uso un modelo de respuesta ContactoLista
@app.get('/contactos', response_model=list[ContactoLista])
def get_all_contactos():
#Devuelve una copia de la lista contactos
    return [x for x in contactos]

@app.get('/') #Ruta
def home():
#Muestra un msj al inicio
    return "Bienvenido! Esta es la página de inicio"

#Devuelvo un contacto según su id (retorna el original)
@app.get('/contacto/{id}') #Ruta de inicio - Parámetro entre {}
def get_contacto_id(id: int): #el parátro que pasamos en la ruta se tiene q´llamar igual {id}=id
    m = contactos[id]
    return m

#Devuelvo un contacto según su id (Retorna una lista de un elemento o un error)
@app.get('/contactos/{id}', response_model=Contacto) #Uso el model Contacto xq devuelvo todos los atributos
def get_contacto(id: int): #el parátro que pasamos en la ruta se tiene q´llamar igual {id}=id
    c = buscar_contacto(id)
    if c is None:
        return Response(content = 'No encontrado!', status_code= status.HTTP_404_NOT_FOUND)
    return c


#Función para buscar un contacto.
def buscar_contacto(id: int):
    encontrados = [x for x in contactos if x.id == id] #Devuelve otra lista
    if len(encontrados) == 0:
        return None #No existe un contacto con ese id.
    if len(encontrados) > 1: #Si hay mas de uno lanzo una excepción.
        raise Exception('Algo está mal: Hay mas de un contacto con el mismo id!')
    return encontrados[0] #Si encontró un solo contacto devuelvo el único elemento




# * Otra forma menos eficiente porque genera otra lista en memoria cada vez que busca:
@app.get('/contactos2/{id}')
def get_contacto_id2(id: int):
    contactos_con_el_id = [m for m in contactos if m.id == id]
    if len(contactos_con_el_id)>0:
        return contactos_con_el_id[0]
    return 404, "Contacto no encontrado"

# ** Otra forma utilizando en vez de una lista un generador (next):
@app.get('/contactos3/{id}')
def get_contacto_id3(id: int):
    #El generador no se ejecuta hasta que le digamos que se ejecute.
    contacto = next((m for m in contactos if m.id == id), None) #Si no encuentra devuelve None
    if contacto:
        return contacto
    return 404, "Contacto no encontrado"

# * y **
#En general este tipo de búsquedas no lo vamos a utilizar porque la búsqueda es tarea de la BBDD
#Lo que vamos a realizar es una request.

#Método post para crear un contacto:
@app.post('/crear_contacto')
def crear_contacto(c: Contacto):
    contactos.append(c) #Agrega un contacto a la lista
    return 201, 'Contacto creado correctamente!' #El return devuelve un cod HTTP y un msj.

#Método post (Otra forma)):
@app.post('/contactos') #Aunque la ruta sea la misma que traer la lista, como tiene un verbo POST se da cuenta que tiene que crear un contacto.
def agregar_contacto(item: ContactoCreate, response: Response):
    try: #Se ejecuta si no hay error
        c=Contacto(id = new_id(), nombre = item.nombre,
            direccion = item.direccion, telefonos = item.telefonos)
        contactos.append(c) #Agrega un contacto a la lista
        response.status_code = status.HTTP_201_CREATED
        return 'Contacto creado correctamente.'
    except Exception as ex: #Si hay un error sale x acá.
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Error al agregar el contacto' + str(ex)

#Generador de id, busca el mas alto en la lista y le suma 1.
def new_id():
    id = 0
    for c in contactos:
        if c.id>id:
            id = c.id
    return id + 1

@app.delete('/contactos/{id}')
def borrar_contacto(id: int):
    try:
        c = buscar_contacto(id) #Busca el contacto en la lista
        if not c: #Si no lo encuentra devuelvo un 404
            return Response(content='No se encuentra el contacto!', status_code=404)
        contactos.remove(c)#Si lo encuentra lo borro y devuelvo un 200
        return Response(content = 'Contacto borrado correctamente', status_code=200)
    except Exception as ex: #Si hubo un error lanzo una excepción y devuelvo un 500
        return 'Error al borrar el contacto: ' + str(ex), 500

#Put reemplaza la instancia completa por la que viene en la Request
@app.put('/contactos/{id}') #Le pasamos el id que tiene que buscar
def modificar_contacto(id: int, item: Contacto): #Tengo un id y además en el cuerpo del Request recibo un ContactoCreate (JSON sin id)
    try: #Toma el id que le pasé en la ruta y busca el contacto
        c: Contacto = buscar_contacto(id)
        if not c: #Si c es None devuelve un response 404.
            return Response('No se encuentra el contacto', 404)
        #Si se encontró un contacto con ese id, reemplazo los valor por los que vienen del JSON (Menos el id que queda igual)
        c.nombre = item.nombre #Reemplaza por el nuevo valor que viene en la Request
        c.direccion= item.direccion #Reemplaza por el nuevo valor que viene en la Request
        c.telefonos = item.telefonos #Reemplaza por el nuevo valor que viene en la Request
        return Response('Contacto modificado correctamente', 200) #Siempre usar Response en las respuestas
    except Exception as ex:
        return Response('Error al modificar el contacto' + str(ex), 500)

#Patch modifica solo los valores que vienen en la request (Hay APIs que no lo implementan)
@app.patch('/contactospatch/{id}') #Le pasamos el id que tiene que buscar
def modificar_contacto(id: int, item: Contacto): #Tengo un id y además en el cuerpo del Request recibo un ContactoCreate (JSON sin id)
    try: #Toma el id que le pasé en la ruta y busca el contacto
        c: Contacto = buscar_contacto(id)
        if not c: #Si c es None devuelve un response 404.
            return Response('No se encuentra el contacto', 404)
        #Si se encontró un contacto con ese id, reemplazo los valor por los que vienen del JSON (Menos el id que queda igual)
        c.nombre = item.nombre #Reemplaza solamente si viene en la Request
        c.direccion= item.direccion #Reemplaza solamente si viene en la Request
        c.telefonos = item.telefonos #Reemplaza solamente si viene en la Request
        return Response('Contacto modificado correctamente', 200) #Siempre usar Response en las respuestas
    except Exception as ex:
        return Response('Error al modificar el contacto' + str(ex), 500)