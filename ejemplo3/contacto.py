from pydantic import BaseModel
from typing import Optional

#ContanctoCreate desciende de BaseModel y eso nos da la
#posibilidad de usar tipos y validarlos en el ingreso
#y en el egreso.
class ContactoCreate(BaseModel):
    nombre: str
    direccion: Optional[str]
    telefonos: Optional[str]

#La clase contacto tiene un solo atributo que es el "id"
#pero hereda de ContactoCreate, esto es para que cuando
#creamos una nueva instancia el endpoint le pasamos como parámetro
#el tipo que queremos pasarle como dato, ese tipo en los endpoint de 
#tipo POST (crear) no deberían tener el id (porque es algo que creamos
# nosotros) Entonces creamos una clase para el "create" que tiene todos
#los atributos, menos el "id". Pero cuando lo quiero devolver con un GET,
#lo que hago es definir otra clase mas (q tambien hereda de BaseModel) y 
# que ya tiene todos los atributos más el "id", entonces hago otra clase
#que hereda de ContactoCreate y le agrega el "id", queda:

class Contacto(ContactoCreate): #Hereda de ContactoCreate y de BaseModel
    id: int

#Clase que hereda de ContactoCreate y muestra lo mismo con pass
class ContactoLista(ContactoCreate):
    pass