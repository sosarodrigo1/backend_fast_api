from lzma import MODE_NORMAL
from yaml import MarkedYAMLError


class Version:
    #inicializador: Metodo de clase (Estático)
    def __init__(self, mayor, menor, patch):
        self.mayor = mayor
        self.menor = menor
        self.nro_de_patch = patch

    # def as_string(self):
    #     return f'V{self.mayor}.{self.menor}.{self.nro_de_patch}'

    def __str__(self):
        return f'V{self.mayor}.{self.menor}.{self.nro_de_patch}'

    def as_dict(self):
        return {
            'mayor': self.mayor,
            'menor': self.menor,
            'patch': self.nro_de_patch
        }