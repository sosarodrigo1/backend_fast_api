from fastapi import FastAPI
from datetime import datetime
from sqlalchemy import true
from version import Version
import uvicorn
from ejemplo1.version import Version
from auto import Auto

app = FastAPI()

@app.get("/")
def read_root():
    return "hola"

@app.get("/hora")
def get_hora():
    return datetime.now()

@app.get("/version")
def get_version():
    v = Version(0,0,1)
    return v.as_dict()

@app.get("/auto")
def get_auto():
    a = Auto('Honda', 'Civic', 1993)
    return a.as_dict()

##Si se ejecuta desde CMD...
if __name__ == "__main__":
    uvicorn.run("api1:app", reload=True) #Reload automatico
