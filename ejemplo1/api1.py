from imp import reload
from version import Version
from datetime import datetime
#5- Importamos uvicorn
import uvicorn
#1-Importamos el módulo de FastAPI:
from fastapi import FastAPI
#2- Creamos una variable de tipo FastAPI, es una instancia de la clase FastAPI
#en python no se pone new, directamente se pone el nombre de la clase
app = FastAPI()

#3- Creamos una función que nos devuelve un string
#4- Ahora la convertimos en un endpoint asignándole un atributo get y su ruta
@app.get("/") #Ruta de inicio
def read_roor():
    return "Que duermas lindo bebecita"

print(__name__)

#7- Permite que no se ejecute el servidor cuando importamos api1.py desde otro lado
if __name__ == "__main__":
    #6- Llamamos a uvicorn
    uvicorn.run("api1:app", reload=True) #De esta forma permite el reload automático

#8- Ejemplo de endpoint para obtener la hora
@app.get('/hora') #Ruta del endpoint
def get_hora():
    return datetime.now()

#9- Ejemplo de endpoint que obtiene un diccinario que luego FastAPI conviernte en JSON
@app.get('/version') #Ruta del endpoint
def get_version():
    v = Version(1,3,67) #Instancio la clase
    return v.as_dict() #Llamo al método as_dict()