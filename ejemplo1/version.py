class Version:
    def __init__(self, mayor, menor, patch):
        self.mayor = mayor
        self.menor = menor
        self.nro_de_patch = patch

#Definimos un método de la clase, para eso tiene que agregarse el self como parámetro
    def __str__(self):
        return f'V{self.mayor}.{self.menor}.{self.nro_de_patch}'

    def as_dict(self):
        return {
            'mayor': self.mayor,
            'menor': self.menor,
            'patch': self.nro_de_patch,
        }