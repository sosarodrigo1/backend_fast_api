# Contacto: clase con atributos
# id, nombre, dirección, telefonos
# hacer una api con endpoints para 
# 1) devolver una lista de contactos fija, 
# 2) el detalle de un contacto, dado su id, 
# 3) permitir agregar un contacto
from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn

class Contacto(BaseModel):
    id: int
    nombre: str
    direccion: str
    telefonos: int

Agenda = [
    Contacto (id=10, nombre ='Enzo', direccion= 'Juan Chassaing', telefonos='3445454736' ),
    Contacto (id=5, nombre ='Juan', direccion= 'Jujuy', telefonos='3444454966' ),
    Contacto (id=2, nombre ='Ernesto', direccion= 'Ruperto Perez', telefonos='3435454733' )
]

