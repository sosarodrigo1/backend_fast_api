# declarar una clase Auto con 
# marca: string
# modelo: string
# anio: integer
# fecha_compra: date
# tipo: string

# agregar un endpoint que cree una instancia de la clase y la devuelva como json

from datetime import datetime

class Auto: #Fecha de compra y tipo están seteados como parametros opcionales con valores x defecto
    def __init__(self, marca, modelo, anio, fecha_compra= datetime.now(), tipo = 'automovil'):
        self.marca = marca
        self.modelo = modelo
        self.anio = anio
        self.fecha_compra = fecha_compra
        self.tipo = tipo

    def as_dict(self):
        return {
            'marca': self.marca,
            'modelo': self.modelo,
            'anio': self.anio,
            'fecha_compra': self.fecha_compra,
            'tipo': self.tipo
        }