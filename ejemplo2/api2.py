from imp import reload
from typing import Optional
import pydantic
from auto import Auto
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from datetime import date

app = FastAPI()

@app.get('/auto') #Ruta de inicio
def get_auto():
    a = Auto('Honda', 'Civic', 1993)
    return a.as_dict()

if __name__ == "__main__":
    #De esta forma obtiene el nombre de la aplicacion
    uvicorn.run(f"{__name__}:app", reload=True)

#Moto hereda de la clase BaseModel de la librería pydantic (Importarla)
class Moto(BaseModel):
    id: int #Agregamos una nueva variable para buscar por parámetro.
    marca: str #De esta forma obligamos a las variables a tener tipo
    modelo: str
    anio: int
    fecha_compra: Optional[date]

@app.get('/moto') #Ruta de inicio
def get_moto():
     #La forma de instanciar es similar a c#
     #Se puede dejar sin parámetros tambien
    m = Moto(
        marca = 'Zanella',
        modelo = '200',
        anio = 1994,
        fecha_compra = date.today()
    )
    return m

#Creo un array de motos:
motos = [
    Moto( id=0, marca = 'Honda', modelo = 'CBR',anio = 2000),
    Moto( id=1, marca = 'Suzuki', modelo = '450',anio = 1998),
    Moto( id=2, marca = 'Ducati', modelo = 'Corso',anio = 2010)
    ]

#Creo un endpoint que me devuelve la segunda moto del array:
@app.get('/moto1') #Ruta de inicio
def get_moto1():
    m = motos[1]
    return m

#Creo un endpoint que me devuelve el array de motos:
@app.get('/motos') #Ruta de inicio
def get_lista_motos():
    return motos

#Creamos un nuevo modelo para esa respuesta
class MotoLista(BaseModel):
    marca: str
    modelo: str

#Creo un endpoint que me devuelve una lista (list) de motos en
# base al nuevo modelo de respuesta:
@app.get('/motos2', response_model=list[MotoLista]) #Ruta de inicio y modelo de response
def get_lista_motos2():
    return motos

#Creo un endpoint que me devuelve la segunda moto según el id:
@app.get('/motos/{id}') #Ruta de inicio - Parámetro entre {}
def get_moto_id(id: int): #el parátro que pasamos en la ruta se tiene q´llamar igual {id}=id
    m = motos[id]
    return m

#FastAPI va a tomar esa carga que viene desde un JSON y
# lo va a convertir en un diccionario, y si el parámetro es
# es del tipo de una clase (Ej: Moto) automáticamente lo convierte
# a una instancia de esa clase.
@app.post('/crear_moto')
def crear_moto(m: Moto): #Recibe una Moto
    #Espera un JSON con los datos para crear una Moto.
    motos.append(m) #Agrega una moto a la lista
    return 201, 'Moto creada correctamente' #El return devuelve un cod HTTP y un msj.